import emitter from './events'

let jewels = []
let numRows = 0
let numCols = 0
emitter.on('jewels', (data) => jewels = data);
emitter.on('gameSize', (data) => {
  numRows = data.rows
  numCols = data.cols
});

export default () => {
    let fellDown = 0;
  
    for(let j = 0; j < numCols; j++) {
      for(let i = numRows - 1; i > 0; i--) {
        if(jewels[i][j] === -1 && jewels[i - 1][j] >= 0) {
          jewels[i][j] = jewels[i - 1][j];
          jewels[i - 1][j] = -1;
          fellDown++;
          emitter.emit('jewels', jewels)
        }
      }
    }
    
    // если падать больше нечему, изменяем состояние игры
    emitter.emit('gameState', 'refill')
}