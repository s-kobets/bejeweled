import updateGameField from './updateGameField'
import emitter from './events'

let selectedRow = -1; // выбранный ряд
let selectedCol = -1; // выбранный столбец
let posX; // столбец второго выбранного гема
let posY; // ряд второго выбранного гема

let gameState = ''
emitter.on('gameState', (state) => {
  gameState = state
})
emitter.on('selectedRow', (row) => {
  selectedRow = row
})

export default (event) => {
  const target = event.currentTarget
  /* клик по гему */
  if (target.className.includes('gem')) {
    /* ожидается выбор гема */
    debugger
    if (gameState === "pick") {
      // определить строку и столбец
      const [row, col] = target.id.match(/[0-9]/g).map(num => parseInt(num, 10))

      // если ни один гем не выбран, сохранить позицию выбранного
      if (selectedRow === -1) {
        selectedRow = row;
        selectedCol = col;
      } else {
        /*
          если какой-то гем уже выбран,
          проверить, что тап был по соседнему гему и поменять их местами
          иначе просто выделить новый гем
        */
        if ((Math.abs(selectedRow - row) === 1 && selectedCol === col) ||
          (Math.abs(selectedCol - col) === 1 && selectedRow === row)) {
          // target.classList.remove('active')

          // сохранить позицию второго выбранного гема
          posX = col;
          posY = row;
          // поменять их местами
          updateGameField({selectedRow, selectedCol}, {posX, posY})
          // переключить состояние игры
          emitter.emit('gameState', 'switch', {selectedRow, selectedCol}, {posX, posY})
        } else {
          selectedRow = row;
          selectedCol = col;
        }
      }
    }
  }
}
