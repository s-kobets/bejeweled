import emitter from './events'
import removeGems from './removeGems'

let jewels = []
let numRows = 0
let numCols = 0
emitter.on('jewels', (data) => jewels = data);
emitter.on('gameSize', (data) => {
  numRows = data.rows
  numCols = data.cols
});

export default () => {
  let gemsPlaced = 0;
  for(let i = 0; i < numCols; i++) {
    if(jewels[0][i] == -1) {
      jewels[0][i] = Math.floor(Math.random() * 8);
      emitter.emit('jewels', jewels)
      gemsPlaced++;
    }
  }
 
  /* если появились новые гемы, проверить, нужно ли опустить что-то вниз */
  if( gemsPlaced ) {
    emitter.emit('gameState', 'descent')
  } else {
    /* если новых гемов не появилось, проверяем поле на группы сбора */
    let combo = 0
    for(let i = 0; i < numRows; i++) {
      for(let j = 0; j < numCols; j++) {
        if(j <= numCols - 3 && jewels[i][j] == jewels[i][j + 1] && jewels[i][j] == jewels[i][j + 2]){
          combo++;
          removeGems(i, j);
        }
        if(i <= numRows - 3 && jewels[i][j] == jewels[i + 1][j] && jewels[i][j] == jewels[i + 2][j]){
          combo++;
          removeGems(i, j);
        }
      }
    }
    // удаляем найденные группы сбора
    if(combo > 0){
      emitter.emit('gameState', 'descent')
    } else { // или вновь запускаем цикл игры
      emitter.emit('gameState', 'pick')
      emitter.emit('selectedRow', -1)
    }
  }
}