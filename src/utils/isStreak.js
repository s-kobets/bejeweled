import emitter from './events'
let jewels = []
emitter.on('jewels', (data) => jewels = data);

export function isVerticalStreak(row, col) {
  let gemValue = jewels[row][col];
  let streak = 0;
  let tmp = row;
  while (tmp > 0 && jewels[tmp - 1][col] === gemValue) {
    streak++;
    tmp--;
  }
  tmp = row;
  // общее количество строк
  const {
    length
  } = jewels
  
  while (tmp < length - 1 && jewels[tmp + 1][col] === gemValue) {
    streak++;
    tmp++;
  }
  return streak > 1
}

export function isHorizontalStreak(row, col) {
  let gemValue = jewels[row][col];
  let streak = 0;
  let tmp = col;
  while (tmp > 0 && jewels[row][tmp - 1] === gemValue) {
    streak++;
    tmp--;
  }
  tmp = col;
  // общее количество столбцов
  const {
    length
  } = jewels[0]
  while (tmp < length - 1 && jewels[row][tmp + 1] === gemValue) {
    streak++;
    tmp++;
  }
  return streak > 1
}

export default function isStreak(row, col) {
  return isVerticalStreak(row, col) || isHorizontalStreak(row, col);
}
