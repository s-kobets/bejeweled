import {
  isVerticalStreak,
  isHorizontalStreak
} from './isStreak'
import emitter from './events'

let jewels = []
let numRows = 0
let numCols = 0
emitter.on('jewels', (data) => jewels = data);
emitter.on('gameSize', (size) => {
  numRows = size.rows
  numCols = size.cols
});

export default (row, col) => {
  let gemValue = jewels[row][col];
  let tmp = row;

  if (isVerticalStreak(row, col)) {
    while (tmp > 0 && jewels[tmp - 1][col] == gemValue) {
      // $("#" + gemIdPrefix + "_" + (tmp - 1) + "_" + col).addClass("remove");
      jewels[tmp - 1][col] = -1;
      tmp--;
    }
    tmp = row;
    while (tmp < numRows - 1 && jewels[tmp + 1][col] == gemValue) {
      // $("#" + gemIdPrefix + "_" + (tmp + 1) + "_" + col).addClass("remove");
      jewels[tmp + 1][col] = -1;
      tmp++;
    }
  }
  if (isHorizontalStreak(row, col)) {
    tmp = col;
    while (tmp > 0 && jewels[row][tmp - 1] == gemValue) {
      // $("#" + gemIdPrefix + "_" + row + "_" + (tmp - 1)).addClass("remove");
      jewels[row][tmp - 1] = -1;
      tmp--;
    }
    tmp = col;
    while (tmp < numCols - 1 && jewels[row][tmp + 1] == gemValue) {
      // $("#" + gemIdPrefix + "_" + row + "_" + (tmp + 1)).addClass("remove");
      jewels[row][tmp + 1] = -1;
      tmp++;
    }
  }
  jewels[row][col] = -1;

  emitter.emit('jewels', jewels)
}
