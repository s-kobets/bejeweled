import emitter from './events'
import isStreak from './isStreak'
import removeGems from './removeGems'
import checkFalling from './checkFalling'
import updateGameField from './updateGameField'
import placeNewGems from './placeNewGems'

let movingItems = 0; // количество передвигаемых в данный момент гемов

let gameState = ''
emitter.on('gameState', (state, selected, pos) => {
  gameState = state
  checkMoving(selected, pos)
});

const checkMoving = (selected, pos) => {
  // movingItems--;
  // когда закончилась анимация последнего гема
  // if (movingItems == 0) {
  // действуем в зависимости от состояния игры
  switch (gameState) {
    // после передвижения гемов проверяем поле на появление групп сбора
    case "switch":
    case "revert":
      // проверяем, появились ли группы сбора
      const {
        selectedRow,
        selectedCol
      } = selected
      const {
        posX,
        posY
      } = pos
      if (!isStreak(
          selectedRow,
          selectedCol
        ) && !isStreak(
          posY,
          posX
        )) {
        // если групп сбора нет, нужно отменить совершенное движение
        // а если действие уже отменяется, то вернуться к исходному состоянию ожидания выбора
        if (gameState !== "revert") {
          emitter.emit('gameState', 'revert', selected, pos)
        } else {
          updateGameField({
            selectedRow,
            selectedCol
          }, {
            posX,
            posY
          })
          emitter.emit('gameState', 'pick')
        }
      } else {
        // если группы сбора есть, нужно их удалить
        if (isStreak(
            selectedRow,
            selectedCol
          )) {
          removeGems(selectedRow, selectedCol);
          emitter.emit('selectedRow', -1)
        }
        if (isStreak(
            posY,
            posX
          )) {
          removeGems(posY, posX);
        }
        emitter.emit('gameState', 'descent')
      }
      break;

    case 'descent': 
      checkFalling();
      emitter.emit('gameState', 'refill')
      break;

    case 'refill': 
      placeNewGems();
      break;
  }
}

export default checkMoving
