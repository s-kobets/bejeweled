import emitter from './events'

let jewels = []
emitter.on('jewels', (data) => jewels = data);

export default (selected, pos) => {
  const {
    selectedRow,
    selectedCol
  } = selected
  const {
    posX,
    posY
  } = pos
   // поменять идентификаторы гемов
   document.getElementById(`gem_${selectedRow}_${selectedCol}`).id = `gem_${posY}_${posX}`
   document.getElementById(`gem_${posY}_${posX}`).id = `gem_${selectedRow}_${selectedCol}`
 
   // поменять гемы в сетке
   let temp = jewels[selectedRow][selectedCol];
   jewels[selectedRow][selectedCol] = jewels[posY][posX];
   jewels[posY][posX] = temp;
   emitter.emit('jewels', jewels)
}