
(function(l, i, v, e) { v = l.createElement(i); v.async = 1; v.src = '//' + (location.host || 'localhost').split(':')[0] + ':35729/livereload.js?snipver=1'; e = l.getElementsByTagName(i)[0]; e.parentNode.insertBefore(v, e)})(document, 'script');
var app = (function () {
    'use strict';

    function noop() { }
    const identity = x => x;
    function assign(tar, src) {
        for (const k in src)
            tar[k] = src[k];
        return tar;
    }
    function add_location(element, file, line, column, char) {
        element.__svelte_meta = {
            loc: { file, line, column, char }
        };
    }
    function run(fn) {
        return fn();
    }
    function blank_object() {
        return Object.create(null);
    }
    function run_all(fns) {
        fns.forEach(run);
    }
    function is_function(thing) {
        return typeof thing === 'function';
    }
    function safe_not_equal(a, b) {
        return a != a ? b == b : a !== b || ((a && typeof a === 'object') || typeof a === 'function');
    }
    const is_client = typeof window !== 'undefined';
    let now = is_client
        ? () => window.performance.now()
        : () => Date.now();
    let raf = is_client ? requestAnimationFrame : noop;

    const tasks = new Set();
    let running = false;
    function run_tasks() {
        tasks.forEach(task => {
            if (!task[0](now())) {
                tasks.delete(task);
                task[1]();
            }
        });
        running = tasks.size > 0;
        if (running)
            raf(run_tasks);
    }
    function loop(fn) {
        let task;
        if (!running) {
            running = true;
            raf(run_tasks);
        }
        return {
            promise: new Promise(fulfil => {
                tasks.add(task = [fn, fulfil]);
            }),
            abort() {
                tasks.delete(task);
            }
        };
    }

    function append(target, node) {
        target.appendChild(node);
    }
    function insert(target, node, anchor) {
        target.insertBefore(node, anchor || null);
    }
    function detach(node) {
        node.parentNode.removeChild(node);
    }
    function destroy_each(iterations, detaching) {
        for (let i = 0; i < iterations.length; i += 1) {
            if (iterations[i])
                iterations[i].d(detaching);
        }
    }
    function element(name) {
        return document.createElement(name);
    }
    function text(data) {
        return document.createTextNode(data);
    }
    function space() {
        return text(' ');
    }
    function empty() {
        return text('');
    }
    function listen(node, event, handler, options) {
        node.addEventListener(event, handler, options);
        return () => node.removeEventListener(event, handler, options);
    }
    function attr(node, attribute, value) {
        if (value == null)
            node.removeAttribute(attribute);
        else
            node.setAttribute(attribute, value);
    }
    function children(element) {
        return Array.from(element.childNodes);
    }
    function set_data(text, data) {
        data = '' + data;
        if (text.data !== data)
            text.data = data;
    }
    function set_style(node, key, value) {
        node.style.setProperty(key, value);
    }
    function toggle_class(element, name, toggle) {
        element.classList[toggle ? 'add' : 'remove'](name);
    }

    let stylesheet;
    let active = 0;
    let current_rules = {};
    // https://github.com/darkskyapp/string-hash/blob/master/index.js
    function hash(str) {
        let hash = 5381;
        let i = str.length;
        while (i--)
            hash = ((hash << 5) - hash) ^ str.charCodeAt(i);
        return hash >>> 0;
    }
    function create_rule(node, a, b, duration, delay, ease, fn, uid = 0) {
        const step = 16.666 / duration;
        let keyframes = '{\n';
        for (let p = 0; p <= 1; p += step) {
            const t = a + (b - a) * ease(p);
            keyframes += p * 100 + `%{${fn(t, 1 - t)}}\n`;
        }
        const rule = keyframes + `100% {${fn(b, 1 - b)}}\n}`;
        const name = `__svelte_${hash(rule)}_${uid}`;
        if (!current_rules[name]) {
            if (!stylesheet) {
                const style = element('style');
                document.head.appendChild(style);
                stylesheet = style.sheet;
            }
            current_rules[name] = true;
            stylesheet.insertRule(`@keyframes ${name} ${rule}`, stylesheet.cssRules.length);
        }
        const animation = node.style.animation || '';
        node.style.animation = `${animation ? `${animation}, ` : ``}${name} ${duration}ms linear ${delay}ms 1 both`;
        active += 1;
        return name;
    }
    function delete_rule(node, name) {
        node.style.animation = (node.style.animation || '')
            .split(', ')
            .filter(name
            ? anim => anim.indexOf(name) < 0 // remove specific animation
            : anim => anim.indexOf('__svelte') === -1 // remove all Svelte animations
        )
            .join(', ');
        if (name && !--active)
            clear_rules();
    }
    function clear_rules() {
        raf(() => {
            if (active)
                return;
            let i = stylesheet.cssRules.length;
            while (i--)
                stylesheet.deleteRule(i);
            current_rules = {};
        });
    }

    let current_component;
    function set_current_component(component) {
        current_component = component;
    }

    const dirty_components = [];
    const resolved_promise = Promise.resolve();
    let update_scheduled = false;
    const binding_callbacks = [];
    const render_callbacks = [];
    const flush_callbacks = [];
    function schedule_update() {
        if (!update_scheduled) {
            update_scheduled = true;
            resolved_promise.then(flush);
        }
    }
    function add_render_callback(fn) {
        render_callbacks.push(fn);
    }
    function flush() {
        const seen_callbacks = new Set();
        do {
            // first, call beforeUpdate functions
            // and update components
            while (dirty_components.length) {
                const component = dirty_components.shift();
                set_current_component(component);
                update(component.$$);
            }
            while (binding_callbacks.length)
                binding_callbacks.shift()();
            // then, once components are updated, call
            // afterUpdate functions. This may cause
            // subsequent updates...
            while (render_callbacks.length) {
                const callback = render_callbacks.pop();
                if (!seen_callbacks.has(callback)) {
                    callback();
                    // ...so guard against infinite loops
                    seen_callbacks.add(callback);
                }
            }
        } while (dirty_components.length);
        while (flush_callbacks.length) {
            flush_callbacks.pop()();
        }
        update_scheduled = false;
    }
    function update($$) {
        if ($$.fragment) {
            $$.update($$.dirty);
            run_all($$.before_render);
            $$.fragment.p($$.dirty, $$.ctx);
            $$.dirty = null;
            $$.after_render.forEach(add_render_callback);
        }
    }

    let promise;
    function wait() {
        if (!promise) {
            promise = Promise.resolve();
            promise.then(() => {
                promise = null;
            });
        }
        return promise;
    }
    let outros;
    function group_outros() {
        outros = {
            remaining: 0,
            callbacks: []
        };
    }
    function check_outros() {
        if (!outros.remaining) {
            run_all(outros.callbacks);
        }
    }
    function on_outro(callback) {
        outros.callbacks.push(callback);
    }
    function create_in_transition(node, fn, params) {
        let config = fn(node, params);
        let running = false;
        let animation_name;
        let task;
        let uid = 0;
        function cleanup() {
            if (animation_name)
                delete_rule(node, animation_name);
        }
        function go() {
            const { delay = 0, duration = 300, easing = identity, tick: tick$$1 = noop, css } = config;
            if (css)
                animation_name = create_rule(node, 0, 1, duration, delay, easing, css, uid++);
            tick$$1(0, 1);
            const start_time = now() + delay;
            const end_time = start_time + duration;
            if (task)
                task.abort();
            running = true;
            task = loop(now$$1 => {
                if (running) {
                    if (now$$1 >= end_time) {
                        tick$$1(1, 0);
                        cleanup();
                        return running = false;
                    }
                    if (now$$1 >= start_time) {
                        const t = easing((now$$1 - start_time) / duration);
                        tick$$1(t, 1 - t);
                    }
                }
                return running;
            });
        }
        let started = false;
        return {
            start() {
                if (started)
                    return;
                delete_rule(node);
                if (typeof config === 'function') {
                    config = config();
                    wait().then(go);
                }
                else {
                    go();
                }
            },
            invalidate() {
                started = false;
            },
            end() {
                if (running) {
                    cleanup();
                    running = false;
                }
            }
        };
    }
    function create_out_transition(node, fn, params) {
        let config = fn(node, params);
        let running = true;
        let animation_name;
        const group = outros;
        group.remaining += 1;
        function go() {
            const { delay = 0, duration = 300, easing = identity, tick: tick$$1 = noop, css } = config;
            if (css)
                animation_name = create_rule(node, 1, 0, duration, delay, easing, css);
            const start_time = now() + delay;
            const end_time = start_time + duration;
            loop(now$$1 => {
                if (running) {
                    if (now$$1 >= end_time) {
                        tick$$1(0, 1);
                        if (!--group.remaining) {
                            // this will result in `end()` being called,
                            // so we don't need to clean up here
                            run_all(group.callbacks);
                        }
                        return false;
                    }
                    if (now$$1 >= start_time) {
                        const t = easing((now$$1 - start_time) / duration);
                        tick$$1(1 - t, t);
                    }
                }
                return running;
            });
        }
        if (typeof config === 'function') {
            wait().then(() => {
                config = config();
                go();
            });
        }
        else {
            go();
        }
        return {
            end(reset) {
                if (reset && config.tick) {
                    config.tick(1, 0);
                }
                if (running) {
                    if (animation_name)
                        delete_rule(node, animation_name);
                    running = false;
                }
            }
        };
    }

    function destroy_block(block, lookup) {
        block.d(1);
        lookup.delete(block.key);
    }
    function outro_and_destroy_block(block, lookup) {
        on_outro(() => {
            destroy_block(block, lookup);
        });
        block.o(1);
    }
    function update_keyed_each(old_blocks, changed, get_key, dynamic, ctx, list, lookup, node, destroy, create_each_block, next, get_context) {
        let o = old_blocks.length;
        let n = list.length;
        let i = o;
        const old_indexes = {};
        while (i--)
            old_indexes[old_blocks[i].key] = i;
        const new_blocks = [];
        const new_lookup = new Map();
        const deltas = new Map();
        i = n;
        while (i--) {
            const child_ctx = get_context(ctx, list, i);
            const key = get_key(child_ctx);
            let block = lookup.get(key);
            if (!block) {
                block = create_each_block(key, child_ctx);
                block.c();
            }
            else if (dynamic) {
                block.p(changed, child_ctx);
            }
            new_lookup.set(key, new_blocks[i] = block);
            if (key in old_indexes)
                deltas.set(key, Math.abs(i - old_indexes[key]));
        }
        const will_move = new Set();
        const did_move = new Set();
        function insert(block) {
            if (block.i)
                block.i(1);
            block.m(node, next);
            lookup.set(block.key, block);
            next = block.first;
            n--;
        }
        while (o && n) {
            const new_block = new_blocks[n - 1];
            const old_block = old_blocks[o - 1];
            const new_key = new_block.key;
            const old_key = old_block.key;
            if (new_block === old_block) {
                // do nothing
                next = new_block.first;
                o--;
                n--;
            }
            else if (!new_lookup.has(old_key)) {
                // remove old block
                destroy(old_block, lookup);
                o--;
            }
            else if (!lookup.has(new_key) || will_move.has(new_key)) {
                insert(new_block);
            }
            else if (did_move.has(old_key)) {
                o--;
            }
            else if (deltas.get(new_key) > deltas.get(old_key)) {
                did_move.add(new_key);
                insert(new_block);
            }
            else {
                will_move.add(old_key);
                o--;
            }
        }
        while (o--) {
            const old_block = old_blocks[o];
            if (!new_lookup.has(old_block.key))
                destroy(old_block, lookup);
        }
        while (n)
            insert(new_blocks[n - 1]);
        return new_blocks;
    }
    function mount_component(component, target, anchor) {
        const { fragment, on_mount, on_destroy, after_render } = component.$$;
        fragment.m(target, anchor);
        // onMount happens after the initial afterUpdate. Because
        // afterUpdate callbacks happen in reverse order (inner first)
        // we schedule onMount callbacks before afterUpdate callbacks
        add_render_callback(() => {
            const new_on_destroy = on_mount.map(run).filter(is_function);
            if (on_destroy) {
                on_destroy.push(...new_on_destroy);
            }
            else {
                // Edge case - component was destroyed immediately,
                // most likely as a result of a binding initialising
                run_all(new_on_destroy);
            }
            component.$$.on_mount = [];
        });
        after_render.forEach(add_render_callback);
    }
    function destroy(component, detaching) {
        if (component.$$) {
            run_all(component.$$.on_destroy);
            component.$$.fragment.d(detaching);
            // TODO null out other refs, including component.$$ (but need to
            // preserve final state?)
            component.$$.on_destroy = component.$$.fragment = null;
            component.$$.ctx = {};
        }
    }
    function make_dirty(component, key) {
        if (!component.$$.dirty) {
            dirty_components.push(component);
            schedule_update();
            component.$$.dirty = blank_object();
        }
        component.$$.dirty[key] = true;
    }
    function init(component, options, instance, create_fragment, not_equal$$1, prop_names) {
        const parent_component = current_component;
        set_current_component(component);
        const props = options.props || {};
        const $$ = component.$$ = {
            fragment: null,
            ctx: null,
            // state
            props: prop_names,
            update: noop,
            not_equal: not_equal$$1,
            bound: blank_object(),
            // lifecycle
            on_mount: [],
            on_destroy: [],
            before_render: [],
            after_render: [],
            context: new Map(parent_component ? parent_component.$$.context : []),
            // everything else
            callbacks: blank_object(),
            dirty: null
        };
        let ready = false;
        $$.ctx = instance
            ? instance(component, props, (key, value) => {
                if ($$.ctx && not_equal$$1($$.ctx[key], $$.ctx[key] = value)) {
                    if ($$.bound[key])
                        $$.bound[key](value);
                    if (ready)
                        make_dirty(component, key);
                }
            })
            : props;
        $$.update();
        ready = true;
        run_all($$.before_render);
        $$.fragment = create_fragment($$.ctx);
        if (options.target) {
            if (options.hydrate) {
                $$.fragment.l(children(options.target));
            }
            else {
                $$.fragment.c();
            }
            if (options.intro && component.$$.fragment.i)
                component.$$.fragment.i();
            mount_component(component, options.target, options.anchor);
            flush();
        }
        set_current_component(parent_component);
    }
    class SvelteComponent {
        $destroy() {
            destroy(this, true);
            this.$destroy = noop;
        }
        $on(type, callback) {
            const callbacks = (this.$$.callbacks[type] || (this.$$.callbacks[type] = []));
            callbacks.push(callback);
            return () => {
                const index = callbacks.indexOf(callback);
                if (index !== -1)
                    callbacks.splice(index, 1);
            };
        }
        $set() {
            // overridden by instance, if it has props
        }
    }
    class SvelteComponentDev extends SvelteComponent {
        constructor(options) {
            if (!options || (!options.target && !options.$$inline)) {
                throw new Error(`'target' is a required option`);
            }
            super();
        }
        $destroy() {
            super.$destroy();
            this.$destroy = () => {
                console.warn(`Component was already destroyed`); // eslint-disable-line no-console
            };
        }
    }

    function cubicOut(t) {
        const f = t - 1.0;
        return f * f * f + 1.0;
    }
    function quintOut(t) {
        return --t * t * t * t * t + 1;
    }

    const EventEmitter = function () {
      this.events = {};
    };

    EventEmitter.prototype.on = function (event, listener) {
      if (typeof this.events[event] !== 'object') {
        this.events[event] = [];
      }

      this.events[event].push(listener);
    };

    EventEmitter.prototype.removeListener = function (event, listener) {
      let idx;

      if (typeof this.events[event] === 'object') {
        idx = indexOf(this.events[event], listener);

        if (idx > -1) {
          this.events[event].splice(idx, 1);
        }
      }
    };

    EventEmitter.prototype.emit = function (event) {
      let i, listeners, length, args = [].slice.call(arguments, 1);

      if (typeof this.events[event] === 'object') {
        listeners = this.events[event].slice();
        length = listeners.length;

        for (i = 0; i < length; i++) {
          listeners[i].apply(this, args);
        }
      }
    };

    var emitter = new EventEmitter();

    let jewels = [];
    emitter.on('jewels', (data) => jewels = data);

    var updateGameField = (selected, pos) => {
      const {
        selectedRow,
        selectedCol
      } = selected;
      const {
        posX,
        posY
      } = pos;
       // поменять идентификаторы гемов
       document.getElementById(`gem_${selectedRow}_${selectedCol}`).id = `gem_${posY}_${posX}`;
       document.getElementById(`gem_${posY}_${posX}`).id = `gem_${selectedRow}_${selectedCol}`;
     
       // поменять гемы в сетке
       let temp = jewels[selectedRow][selectedCol];
       jewels[selectedRow][selectedCol] = jewels[posY][posX];
       jewels[posY][posX] = temp;
       emitter.emit('jewels', jewels);
    };

    let selectedRow = -1; // выбранный ряд
    let selectedCol = -1; // выбранный столбец
    let posX; // столбец второго выбранного гема
    let posY; // ряд второго выбранного гема

    let gameState = '';
    emitter.on('gameState', (state) => {
      gameState = state;
    });
    emitter.on('selectedRow', (row) => {
      selectedRow = row;
    });

    var tapHandler = (event) => {
      const target = event.currentTarget;
      /* клик по гему */
      if (target.className.includes('gem')) {
        /* ожидается выбор гема */
        debugger
        if (gameState === "pick") {
          // определить строку и столбец
          const [row, col] = target.id.match(/[0-9]/g).map(num => parseInt(num, 10));

          // если ни один гем не выбран, сохранить позицию выбранного
          if (selectedRow === -1) {
            selectedRow = row;
            selectedCol = col;
          } else {
            /*
              если какой-то гем уже выбран,
              проверить, что тап был по соседнему гему и поменять их местами
              иначе просто выделить новый гем
            */
            if ((Math.abs(selectedRow - row) === 1 && selectedCol === col) ||
              (Math.abs(selectedCol - col) === 1 && selectedRow === row)) {
              // target.classList.remove('active')

              // сохранить позицию второго выбранного гема
              posX = col;
              posY = row;
              // поменять их местами
              updateGameField({selectedRow, selectedCol}, {posX, posY});
              // переключить состояние игры
              emitter.emit('gameState', 'switch', {selectedRow, selectedCol}, {posX, posY});
            } else {
              selectedRow = row;
              selectedCol = col;
            }
          }
        }
      }
    };

    /*! *****************************************************************************
    Copyright (c) Microsoft Corporation. All rights reserved.
    Licensed under the Apache License, Version 2.0 (the "License"); you may not use
    this file except in compliance with the License. You may obtain a copy of the
    License at http://www.apache.org/licenses/LICENSE-2.0

    THIS CODE IS PROVIDED ON AN *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
    KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
    WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
    MERCHANTABLITY OR NON-INFRINGEMENT.

    See the Apache Version 2.0 License for specific language governing permissions
    and limitations under the License.
    ***************************************************************************** */

    function __rest(s, e) {
        var t = {};
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
            t[p] = s[p];
        if (s != null && typeof Object.getOwnPropertySymbols === "function")
            for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) if (e.indexOf(p[i]) < 0)
                t[p[i]] = s[p[i]];
        return t;
    }
    function crossfade(_a) {
        var { fallback } = _a, defaults = __rest(_a, ["fallback"]);
        const to_receive = new Map();
        const to_send = new Map();
        function crossfade(from, node, params) {
            const { delay = 0, duration = d => Math.sqrt(d) * 30, easing = cubicOut } = assign(assign({}, defaults), params);
            const to = node.getBoundingClientRect();
            const dx = from.left - to.left;
            const dy = from.top - to.top;
            const d = Math.sqrt(dx * dx + dy * dy);
            const style = getComputedStyle(node);
            const transform = style.transform === 'none' ? '' : style.transform;
            const opacity = +style.opacity;
            return {
                delay,
                duration: is_function(duration) ? duration(d) : duration,
                easing,
                css: (t, u) => `
				opacity: ${t * opacity};
				transform: ${transform} translate(${u * dx}px,${u * dy}px);
			`
            };
        }
        function transition(items, counterparts, intro) {
            return (node, params) => {
                items.set(params.key, {
                    rect: node.getBoundingClientRect()
                });
                return () => {
                    if (counterparts.has(params.key)) {
                        const { rect } = counterparts.get(params.key);
                        counterparts.delete(params.key);
                        return crossfade(rect, node, params);
                    }
                    // if the node is disappearing altogether
                    // (i.e. wasn't claimed by the other list)
                    // then we need to supply an outro
                    items.delete(params.key);
                    return fallback && fallback(node, params, intro);
                };
            };
        }
        return [
            transition(to_send, to_receive, false),
            transition(to_receive, to_send, true)
        ];
    }

    /* src/Gem.svelte generated by Svelte v3.4.4 */

    const file = "src/Gem.svelte";

    function create_fragment(ctx) {
    	var div, div_id_value, div_intro, div_outro, current, dispose;

    	return {
    		c: function create() {
    			div = element("div");
    			set_style(div, "background-color", ctx.bgColor);
    			set_style(div, "top", "" + (ctx.row * ctx.gemSize + 4) + "px");
    			set_style(div, "left", "" + (ctx.column * ctx.gemSize + 4) + "px");
    			attr(div, "field", ctx.field);
    			div.className = "" + gemClass + " svelte-p6uqg8";
    			div.id = div_id_value = "gem_" + ctx.row + "_" + ctx.column;
    			toggle_class(div, "active", ctx.active);
    			toggle_class(div, "remove", ctx.remove);
    			add_location(div, file, 0, 0, 0);
    			dispose = listen(div, "click", ctx.click_handler);
    		},

    		l: function claim(nodes) {
    			throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
    		},

    		m: function mount(target, anchor) {
    			insert(target, div, anchor);
    			current = true;
    		},

    		p: function update(changed, ctx) {
    			if (!current || changed.bgColor) {
    				set_style(div, "background-color", ctx.bgColor);
    			}

    			if (!current || changed.row || changed.gemSize) {
    				set_style(div, "top", "" + (ctx.row * ctx.gemSize + 4) + "px");
    			}

    			if (!current || changed.column || changed.gemSize) {
    				set_style(div, "left", "" + (ctx.column * ctx.gemSize + 4) + "px");
    			}

    			if (!current || changed.field) {
    				attr(div, "field", ctx.field);
    			}

    			if ((!current || changed.row || changed.column) && div_id_value !== (div_id_value = "gem_" + ctx.row + "_" + ctx.column)) {
    				div.id = div_id_value;
    			}

    			if ((changed.gemClass || changed.active)) {
    				toggle_class(div, "active", ctx.active);
    			}

    			if ((changed.gemClass || changed.remove)) {
    				toggle_class(div, "remove", ctx.remove);
    			}
    		},

    		i: function intro(local) {
    			if (current) return;
    			add_render_callback(() => {
    				if (div_outro) div_outro.end(1);
    				if (!div_intro) div_intro = create_in_transition(div, ctx.receive, {key: ctx.field});
    				div_intro.start();
    			});

    			current = true;
    		},

    		o: function outro(local) {
    			if (div_intro) div_intro.invalidate();

    			if (local) {
    				div_outro = create_out_transition(div, ctx.send, {key: ctx.field});
    			}

    			current = false;
    		},

    		d: function destroy(detaching) {
    			if (detaching) {
    				detach(div);
    				if (div_outro) div_outro.end();
    			}

    			dispose();
    		}
    	};
    }

    let gemClass = "gem";

    function instance($$self, $$props, $$invalidate) {
    	
      
      let { row, column, field, gemSize, bgColor, active, remove, onClick } = $$props; // класс элементов-гемов

      function onClickGem(e) {
        onClick(row, column);
        tapHandler(e);
      }

      const [send, receive] = crossfade({
    		duration: d => Math.sqrt(d * 200),

    		fallback(node, params) {
    			const style = getComputedStyle(node);
    			const transform = style.transform === 'none' ? '' : style.transform;

    			return {
    				duration: 600,
    				easing: quintOut,
    				css: t => `
					transform: ${transform} scale(${t});
					opacity: ${t}
				`
    			};
    		}
    	});

    	const writable_props = ['row', 'column', 'field', 'gemSize', 'bgColor', 'active', 'remove', 'onClick'];
    	Object.keys($$props).forEach(key => {
    		if (!writable_props.includes(key) && !key.startsWith('$$')) console.warn(`<Gem> was created with unknown prop '${key}'`);
    	});

    	function click_handler(e) {
    		return onClickGem(e);
    	}

    	$$self.$set = $$props => {
    		if ('row' in $$props) $$invalidate('row', row = $$props.row);
    		if ('column' in $$props) $$invalidate('column', column = $$props.column);
    		if ('field' in $$props) $$invalidate('field', field = $$props.field);
    		if ('gemSize' in $$props) $$invalidate('gemSize', gemSize = $$props.gemSize);
    		if ('bgColor' in $$props) $$invalidate('bgColor', bgColor = $$props.bgColor);
    		if ('active' in $$props) $$invalidate('active', active = $$props.active);
    		if ('remove' in $$props) $$invalidate('remove', remove = $$props.remove);
    		if ('onClick' in $$props) $$invalidate('onClick', onClick = $$props.onClick);
    	};

    	return {
    		row,
    		column,
    		field,
    		gemSize,
    		bgColor,
    		active,
    		remove,
    		onClick,
    		onClickGem,
    		send,
    		receive,
    		click_handler
    	};
    }

    class Gem extends SvelteComponentDev {
    	constructor(options) {
    		super(options);
    		init(this, options, instance, create_fragment, safe_not_equal, ["row", "column", "field", "gemSize", "bgColor", "active", "remove", "onClick"]);

    		const { ctx } = this.$$;
    		const props = options.props || {};
    		if (ctx.row === undefined && !('row' in props)) {
    			console.warn("<Gem> was created without expected prop 'row'");
    		}
    		if (ctx.column === undefined && !('column' in props)) {
    			console.warn("<Gem> was created without expected prop 'column'");
    		}
    		if (ctx.field === undefined && !('field' in props)) {
    			console.warn("<Gem> was created without expected prop 'field'");
    		}
    		if (ctx.gemSize === undefined && !('gemSize' in props)) {
    			console.warn("<Gem> was created without expected prop 'gemSize'");
    		}
    		if (ctx.bgColor === undefined && !('bgColor' in props)) {
    			console.warn("<Gem> was created without expected prop 'bgColor'");
    		}
    		if (ctx.active === undefined && !('active' in props)) {
    			console.warn("<Gem> was created without expected prop 'active'");
    		}
    		if (ctx.remove === undefined && !('remove' in props)) {
    			console.warn("<Gem> was created without expected prop 'remove'");
    		}
    		if (ctx.onClick === undefined && !('onClick' in props)) {
    			console.warn("<Gem> was created without expected prop 'onClick'");
    		}
    	}

    	get row() {
    		throw new Error("<Gem>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	set row(value) {
    		throw new Error("<Gem>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	get column() {
    		throw new Error("<Gem>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	set column(value) {
    		throw new Error("<Gem>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	get field() {
    		throw new Error("<Gem>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	set field(value) {
    		throw new Error("<Gem>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	get gemSize() {
    		throw new Error("<Gem>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	set gemSize(value) {
    		throw new Error("<Gem>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	get bgColor() {
    		throw new Error("<Gem>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	set bgColor(value) {
    		throw new Error("<Gem>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	get active() {
    		throw new Error("<Gem>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	set active(value) {
    		throw new Error("<Gem>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	get remove() {
    		throw new Error("<Gem>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	set remove(value) {
    		throw new Error("<Gem>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	get onClick() {
    		throw new Error("<Gem>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	set onClick(value) {
    		throw new Error("<Gem>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}
    }

    /* src/Animate.svelte generated by Svelte v3.4.4 */

    function get_each_context(ctx, list, i) {
    	const child_ctx = Object.create(ctx);
    	child_ctx.n = list[i];
    	return child_ctx;
    }

    // (1:0) {#each list as n (n)}
    function create_each_block(key_1, ctx) {
    	var first, current;

    	var gem = new Gem({
    		props: {
    		row: 1,
    		column: ctx.n,
    		field: ctx.n,
    		gemSize: 64,
    		bgColor: "orange",
    		active: ctx.pick === `${1}_${ctx.n}` && ctx.n > 0,
    		onClick: ctx.func
    	},
    		$$inline: true
    	});

    	return {
    		key: key_1,

    		first: null,

    		c: function create() {
    			first = empty();
    			gem.$$.fragment.c();
    			this.first = first;
    		},

    		m: function mount(target, anchor) {
    			insert(target, first, anchor);
    			mount_component(gem, target, anchor);
    			current = true;
    		},

    		p: function update(changed, ctx) {
    			var gem_changes = {};
    			if (changed.list) gem_changes.column = ctx.n;
    			if (changed.list) gem_changes.field = ctx.n;
    			if (changed.pick || changed.list) gem_changes.active = ctx.pick === `${1}_${ctx.n}` && ctx.n > 0;
    			gem.$set(gem_changes);
    		},

    		i: function intro(local) {
    			if (current) return;
    			gem.$$.fragment.i(local);

    			current = true;
    		},

    		o: function outro(local) {
    			gem.$$.fragment.o(local);
    			current = false;
    		},

    		d: function destroy(detaching) {
    			if (detaching) {
    				detach(first);
    			}

    			gem.$destroy(detaching);
    		}
    	};
    }

    function create_fragment$1(ctx) {
    	var each_blocks = [], each_1_lookup = new Map(), each_1_anchor, current;

    	var each_value = ctx.list;

    	const get_key = ctx => ctx.n;

    	for (var i = 0; i < each_value.length; i += 1) {
    		let child_ctx = get_each_context(ctx, each_value, i);
    		let key = get_key(child_ctx);
    		each_1_lookup.set(key, each_blocks[i] = create_each_block(key, child_ctx));
    	}

    	return {
    		c: function create() {
    			for (i = 0; i < each_blocks.length; i += 1) each_blocks[i].c();

    			each_1_anchor = empty();
    		},

    		l: function claim(nodes) {
    			throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
    		},

    		m: function mount(target, anchor) {
    			for (i = 0; i < each_blocks.length; i += 1) each_blocks[i].m(target, anchor);

    			insert(target, each_1_anchor, anchor);
    			current = true;
    		},

    		p: function update(changed, ctx) {
    			const each_value = ctx.list;

    			group_outros();
    			each_blocks = update_keyed_each(each_blocks, changed, get_key, 1, ctx, each_value, each_1_lookup, each_1_anchor.parentNode, outro_and_destroy_block, create_each_block, each_1_anchor, get_each_context);
    			check_outros();
    		},

    		i: function intro(local) {
    			if (current) return;
    			for (var i = 0; i < each_value.length; i += 1) each_blocks[i].i();

    			current = true;
    		},

    		o: function outro(local) {
    			for (i = 0; i < each_blocks.length; i += 1) each_blocks[i].o();

    			current = false;
    		},

    		d: function destroy(detaching) {
    			for (i = 0; i < each_blocks.length; i += 1) each_blocks[i].d(detaching);

    			if (detaching) {
    				detach(each_1_anchor);
    			}
    		}
    	};
    }

    function instance$1($$self, $$props, $$invalidate) {
    	

      let pick = '';

    	let list = [1, 2, 3];

    	function func(row, column) {
    		const $$result = pick = `${row}_${column}`;
    		$$invalidate('pick', pick);
    		return $$result;
    	}

    	return { pick, list, func };
    }

    class Animate extends SvelteComponentDev {
    	constructor(options) {
    		super(options);
    		init(this, options, instance$1, create_fragment$1, safe_not_equal, []);
    	}
    }

    /* src/Counter.svelte generated by Svelte v3.4.4 */

    const file$1 = "src/Counter.svelte";

    function create_fragment$2(ctx) {
    	var p, t0, t1, t2, button0, t4, button1, dispose;

    	return {
    		c: function create() {
    			p = element("p");
    			t0 = text("Count: ");
    			t1 = text(ctx.count);
    			t2 = space();
    			button0 = element("button");
    			button0.textContent = "+1";
    			t4 = space();
    			button1 = element("button");
    			button1.textContent = "-1";
    			add_location(p, file$1, 0, 0, 0);
    			add_location(button0, file$1, 1, 0, 22);
    			add_location(button1, file$1, 2, 0, 70);

    			dispose = [
    				listen(button0, "click", ctx.click_handler),
    				listen(button1, "click", ctx.click_handler_1)
    			];
    		},

    		l: function claim(nodes) {
    			throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
    		},

    		m: function mount(target, anchor) {
    			insert(target, p, anchor);
    			append(p, t0);
    			append(p, t1);
    			insert(target, t2, anchor);
    			insert(target, button0, anchor);
    			insert(target, t4, anchor);
    			insert(target, button1, anchor);
    		},

    		p: function update(changed, ctx) {
    			if (changed.count) {
    				set_data(t1, ctx.count);
    			}
    		},

    		i: noop,
    		o: noop,

    		d: function destroy(detaching) {
    			if (detaching) {
    				detach(p);
    				detach(t2);
    				detach(button0);
    				detach(t4);
    				detach(button1);
    			}

    			run_all(dispose);
    		}
    	};
    }

    function instance$2($$self, $$props, $$invalidate) {
    	let count = 3;

    	function click_handler() {
    		const $$result = count += 1;
    		$$invalidate('count', count);
    		return $$result;
    	}

    	function click_handler_1() {
    		const $$result = count -= 1;
    		$$invalidate('count', count);
    		return $$result;
    	}

    	return { count, click_handler, click_handler_1 };
    }

    class Counter extends SvelteComponentDev {
    	constructor(options) {
    		super(options);
    		init(this, options, instance$2, create_fragment$2, safe_not_equal, []);
    	}
    }

    let jewels$1 = [];
    emitter.on('jewels', (data) => jewels$1 = data);

    function isVerticalStreak(row, col) {
      let gemValue = jewels$1[row][col];
      let streak = 0;
      let tmp = row;
      while (tmp > 0 && jewels$1[tmp - 1][col] === gemValue) {
        streak++;
        tmp--;
      }
      tmp = row;
      // общее количество строк
      const {
        length
      } = jewels$1;
      
      while (tmp < length - 1 && jewels$1[tmp + 1][col] === gemValue) {
        streak++;
        tmp++;
      }
      return streak > 1
    }

    function isHorizontalStreak(row, col) {
      let gemValue = jewels$1[row][col];
      let streak = 0;
      let tmp = col;
      while (tmp > 0 && jewels$1[row][tmp - 1] === gemValue) {
        streak++;
        tmp--;
      }
      tmp = col;
      // общее количество столбцов
      const {
        length
      } = jewels$1[0];
      while (tmp < length - 1 && jewels$1[row][tmp + 1] === gemValue) {
        streak++;
        tmp++;
      }
      return streak > 1
    }

    function isStreak(row, col) {
      return isVerticalStreak(row, col) || isHorizontalStreak(row, col);
    }

    let jewels$2 = [];
    let numRows = 0;
    let numCols = 0;
    emitter.on('jewels', (data) => jewels$2 = data);
    emitter.on('gameSize', (size) => {
      numRows = size.rows;
      numCols = size.cols;
    });

    var removeGems = (row, col) => {
      let gemValue = jewels$2[row][col];
      let tmp = row;

      if (isVerticalStreak(row, col)) {
        while (tmp > 0 && jewels$2[tmp - 1][col] == gemValue) {
          // $("#" + gemIdPrefix + "_" + (tmp - 1) + "_" + col).addClass("remove");
          jewels$2[tmp - 1][col] = -1;
          tmp--;
        }
        tmp = row;
        while (tmp < numRows - 1 && jewels$2[tmp + 1][col] == gemValue) {
          // $("#" + gemIdPrefix + "_" + (tmp + 1) + "_" + col).addClass("remove");
          jewels$2[tmp + 1][col] = -1;
          tmp++;
        }
      }
      if (isHorizontalStreak(row, col)) {
        tmp = col;
        while (tmp > 0 && jewels$2[row][tmp - 1] == gemValue) {
          // $("#" + gemIdPrefix + "_" + row + "_" + (tmp - 1)).addClass("remove");
          jewels$2[row][tmp - 1] = -1;
          tmp--;
        }
        tmp = col;
        while (tmp < numCols - 1 && jewels$2[row][tmp + 1] == gemValue) {
          // $("#" + gemIdPrefix + "_" + row + "_" + (tmp + 1)).addClass("remove");
          jewels$2[row][tmp + 1] = -1;
          tmp++;
        }
      }
      jewels$2[row][col] = -1;

      emitter.emit('jewels', jewels$2);
    };

    let jewels$3 = [];
    let numRows$1 = 0;
    let numCols$1 = 0;
    emitter.on('jewels', (data) => jewels$3 = data);
    emitter.on('gameSize', (data) => {
      numRows$1 = data.rows;
      numCols$1 = data.cols;
    });

    var checkFalling = () => {
      
        for(let j = 0; j < numCols$1; j++) {
          for(let i = numRows$1 - 1; i > 0; i--) {
            if(jewels$3[i][j] === -1 && jewels$3[i - 1][j] >= 0) {
              jewels$3[i][j] = jewels$3[i - 1][j];
              jewels$3[i - 1][j] = -1;
              emitter.emit('jewels', jewels$3);
            }
          }
        }
        
        // если падать больше нечему, изменяем состояние игры
        emitter.emit('gameState', 'refill');
    };

    let jewels$4 = [];
    let numRows$2 = 0;
    let numCols$2 = 0;
    emitter.on('jewels', (data) => jewels$4 = data);
    emitter.on('gameSize', (data) => {
      numRows$2 = data.rows;
      numCols$2 = data.cols;
    });

    var placeNewGems = () => {
      let gemsPlaced = 0;
      for(let i = 0; i < numCols$2; i++) {
        if(jewels$4[0][i] == -1) {
          jewels$4[0][i] = Math.floor(Math.random() * 8);
          emitter.emit('jewels', jewels$4);
          gemsPlaced++;
        }
      }
     
      /* если появились новые гемы, проверить, нужно ли опустить что-то вниз */
      if( gemsPlaced ) {
        emitter.emit('gameState', 'descent');
      } else {
        /* если новых гемов не появилось, проверяем поле на группы сбора */
        let combo = 0;
        for(let i = 0; i < numRows$2; i++) {
          for(let j = 0; j < numCols$2; j++) {
            if(j <= numCols$2 - 3 && jewels$4[i][j] == jewels$4[i][j + 1] && jewels$4[i][j] == jewels$4[i][j + 2]){
              combo++;
              removeGems(i, j);
            }
            if(i <= numRows$2 - 3 && jewels$4[i][j] == jewels$4[i + 1][j] && jewels$4[i][j] == jewels$4[i + 2][j]){
              combo++;
              removeGems(i, j);
            }
          }
        }
        // удаляем найденные группы сбора
        if(combo > 0){
          emitter.emit('gameState', 'descent');
        } else { // или вновь запускаем цикл игры
          emitter.emit('gameState', 'pick');
          emitter.emit('selectedRow', -1);
        }
      }
    };

    let gameState$1 = '';
    emitter.on('gameState', (state, selected, pos) => {
      gameState$1 = state;
      checkMoving(selected, pos);
    });

    const checkMoving = (selected, pos) => {
      // movingItems--;
      // когда закончилась анимация последнего гема
      // if (movingItems == 0) {
      // действуем в зависимости от состояния игры
      switch (gameState$1) {
        // после передвижения гемов проверяем поле на появление групп сбора
        case "switch":
        case "revert":
          // проверяем, появились ли группы сбора
          const {
            selectedRow,
            selectedCol
          } = selected;
          const {
            posX,
            posY
          } = pos;
          if (!isStreak(
              selectedRow,
              selectedCol
            ) && !isStreak(
              posY,
              posX
            )) {
            // если групп сбора нет, нужно отменить совершенное движение
            // а если действие уже отменяется, то вернуться к исходному состоянию ожидания выбора
            if (gameState$1 !== "revert") {
              emitter.emit('gameState', 'revert', selected, pos);
            } else {
              updateGameField({
                selectedRow,
                selectedCol
              }, {
                posX,
                posY
              });
              emitter.emit('gameState', 'pick');
            }
          } else {
            // если группы сбора есть, нужно их удалить
            if (isStreak(
                selectedRow,
                selectedCol
              )) {
              removeGems(selectedRow, selectedCol);
              emitter.emit('selectedRow', -1);
            }
            if (isStreak(
                posY,
                posX
              )) {
              removeGems(posY, posX);
            }
            emitter.emit('gameState', 'descent');
          }
          break;

        case 'descent': 
          checkFalling();
          emitter.emit('gameState', 'refill');
          break;

        case 'refill': 
          placeNewGems();
          break;
      }
    };

    /* src/GameField.svelte generated by Svelte v3.4.4 */

    const file$2 = "src/GameField.svelte";

    function get_each_context_1(ctx, list, i) {
    	const child_ctx = Object.create(ctx);
    	child_ctx.field = list[i];
    	child_ctx.j = i;
    	return child_ctx;
    }

    function get_each_context$1(ctx, list, i) {
    	const child_ctx = Object.create(ctx);
    	child_ctx.jewel = list[i];
    	child_ctx.i = i;
    	return child_ctx;
    }

    // (3:4) {#each jewel as field, j}
    function create_each_block_1(ctx) {
    	var current;

    	var gem = new Gem({
    		props: {
    		row: ctx.i,
    		column: ctx.j,
    		field: ctx.field,
    		gemSize: gemSize,
    		bgColor: ctx.field < 0 ? 'transparent' : ctx.bgColors[ctx.field],
    		remove: ctx.field < 0,
    		active: ctx.pick === `${ctx.i}_${ctx.j}` && ctx.field > 0,
    		onClick: ctx.func
    	},
    		$$inline: true
    	});

    	return {
    		c: function create() {
    			gem.$$.fragment.c();
    		},

    		m: function mount(target, anchor) {
    			mount_component(gem, target, anchor);
    			current = true;
    		},

    		p: function update(changed, ctx) {
    			var gem_changes = {};
    			if (changed.jewels) gem_changes.field = ctx.field;
    			if (changed.gemSize) gem_changes.gemSize = gemSize;
    			if (changed.jewels || changed.bgColors) gem_changes.bgColor = ctx.field < 0 ? 'transparent' : ctx.bgColors[ctx.field];
    			if (changed.jewels) gem_changes.remove = ctx.field < 0;
    			if (changed.pick || changed.jewels) gem_changes.active = ctx.pick === `${ctx.i}_${ctx.j}` && ctx.field > 0;
    			gem.$set(gem_changes);
    		},

    		i: function intro(local) {
    			if (current) return;
    			gem.$$.fragment.i(local);

    			current = true;
    		},

    		o: function outro(local) {
    			gem.$$.fragment.o(local);
    			current = false;
    		},

    		d: function destroy(detaching) {
    			gem.$destroy(detaching);
    		}
    	};
    }

    // (2:2) {#each jewels as jewel, i}
    function create_each_block$1(ctx) {
    	var each_1_anchor, current;

    	var each_value_1 = ctx.jewel;

    	var each_blocks = [];

    	for (var i_1 = 0; i_1 < each_value_1.length; i_1 += 1) {
    		each_blocks[i_1] = create_each_block_1(get_each_context_1(ctx, each_value_1, i_1));
    	}

    	function outro_block(i, detaching, local) {
    		if (each_blocks[i]) {
    			if (detaching) {
    				on_outro(() => {
    					each_blocks[i].d(detaching);
    					each_blocks[i] = null;
    				});
    			}

    			each_blocks[i].o(local);
    		}
    	}

    	return {
    		c: function create() {
    			for (var i_1 = 0; i_1 < each_blocks.length; i_1 += 1) {
    				each_blocks[i_1].c();
    			}

    			each_1_anchor = empty();
    		},

    		m: function mount(target, anchor) {
    			for (var i_1 = 0; i_1 < each_blocks.length; i_1 += 1) {
    				each_blocks[i_1].m(target, anchor);
    			}

    			insert(target, each_1_anchor, anchor);
    			current = true;
    		},

    		p: function update(changed, ctx) {
    			if (changed.jewels || changed.gemSize || changed.bgColors || changed.pick) {
    				each_value_1 = ctx.jewel;

    				for (var i_1 = 0; i_1 < each_value_1.length; i_1 += 1) {
    					const child_ctx = get_each_context_1(ctx, each_value_1, i_1);

    					if (each_blocks[i_1]) {
    						each_blocks[i_1].p(changed, child_ctx);
    						each_blocks[i_1].i(1);
    					} else {
    						each_blocks[i_1] = create_each_block_1(child_ctx);
    						each_blocks[i_1].c();
    						each_blocks[i_1].i(1);
    						each_blocks[i_1].m(each_1_anchor.parentNode, each_1_anchor);
    					}
    				}

    				group_outros();
    				for (; i_1 < each_blocks.length; i_1 += 1) outro_block(i_1, 1, 1);
    				check_outros();
    			}
    		},

    		i: function intro(local) {
    			if (current) return;
    			for (var i_1 = 0; i_1 < each_value_1.length; i_1 += 1) each_blocks[i_1].i();

    			current = true;
    		},

    		o: function outro(local) {
    			each_blocks = each_blocks.filter(Boolean);
    			for (let i_1 = 0; i_1 < each_blocks.length; i_1 += 1) outro_block(i_1, 0);

    			current = false;
    		},

    		d: function destroy(detaching) {
    			destroy_each(each_blocks, detaching);

    			if (detaching) {
    				detach(each_1_anchor);
    			}
    		}
    	};
    }

    function create_fragment$3(ctx) {
    	var div, current;

    	var each_value = ctx.jewels;

    	var each_blocks = [];

    	for (var i = 0; i < each_value.length; i += 1) {
    		each_blocks[i] = create_each_block$1(get_each_context$1(ctx, each_value, i));
    	}

    	function outro_block(i, detaching, local) {
    		if (each_blocks[i]) {
    			if (detaching) {
    				on_outro(() => {
    					each_blocks[i].d(detaching);
    					each_blocks[i] = null;
    				});
    			}

    			each_blocks[i].o(local);
    		}
    	}

    	return {
    		c: function create() {
    			div = element("div");

    			for (var i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].c();
    			}
    			div.id = "gamefield";
    			set_style(div, "width", "" + numCols$3 * gemSize + "px");
    			set_style(div, "height", "" + numRows$3 * gemSize + "px");
    			div.className = "svelte-1rma6ou";
    			add_location(div, file$2, 0, 0, 0);
    		},

    		l: function claim(nodes) {
    			throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
    		},

    		m: function mount(target, anchor) {
    			insert(target, div, anchor);

    			for (var i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].m(div, null);
    			}

    			current = true;
    		},

    		p: function update(changed, ctx) {
    			if (changed.jewels || changed.gemSize || changed.bgColors || changed.pick) {
    				each_value = ctx.jewels;

    				for (var i = 0; i < each_value.length; i += 1) {
    					const child_ctx = get_each_context$1(ctx, each_value, i);

    					if (each_blocks[i]) {
    						each_blocks[i].p(changed, child_ctx);
    						each_blocks[i].i(1);
    					} else {
    						each_blocks[i] = create_each_block$1(child_ctx);
    						each_blocks[i].c();
    						each_blocks[i].i(1);
    						each_blocks[i].m(div, null);
    					}
    				}

    				group_outros();
    				for (; i < each_blocks.length; i += 1) outro_block(i, 1, 1);
    				check_outros();
    			}

    			if (!current || changed.numCols || changed.gemSize) {
    				set_style(div, "width", "" + numCols$3 * gemSize + "px");
    			}

    			if (!current || changed.numRows || changed.gemSize) {
    				set_style(div, "height", "" + numRows$3 * gemSize + "px");
    			}
    		},

    		i: function intro(local) {
    			if (current) return;
    			for (var i = 0; i < each_value.length; i += 1) each_blocks[i].i();

    			current = true;
    		},

    		o: function outro(local) {
    			each_blocks = each_blocks.filter(Boolean);
    			for (let i = 0; i < each_blocks.length; i += 1) outro_block(i, 0);

    			current = false;
    		},

    		d: function destroy(detaching) {
    			if (detaching) {
    				detach(div);
    			}

    			destroy_each(each_blocks, detaching);
    		}
    	};
    }

    let gemSize = 64;

    let numRows$3 = 6;

    let numCols$3 = 7;

    function instance$3($$self, $$props, $$invalidate) {
    	 // количество колонок
      let jewels = new Array(); // двумерный массив гемов на поле
      let pick = ''; // активная ячейка

      emitter.emit('gameState', 'pick');
      emitter.emit('gameSize', {rows: numRows$3, cols: numCols$3});

      /* цвета гемов */
      let bgColors = new Array(
        "magenta",
        "mediumblue",
        "yellow",
        "lime",
        "cyan",
        "orange",
        "crimson",
        "gray"
      );

      /* генерация исходного набора гемов */
      for(let i = 0; i < numRows$3; i++){
        jewels[i] = new Array(); $$invalidate('jewels', jewels);
        for(let j = 0; j < numCols$3; j++){
          /*
            Cоздать гем со случайным цветом
            записать его в сетку jewels,
            создать DOM-элемент, раскрасить его и поместить его на поле
          */
          do {
            jewels[i][j] = Math.floor(Math.random() * 8); $$invalidate('jewels', jewels);
            emitter.emit('jewels', jewels);
          } while (isStreak(i, j))
        }
      }

      emitter.on('jewels', (data) => {
        console.log('jewels', data);
        $$invalidate('jewels', jewels = data);
      });

      emitter.on('gameState', (state, selected, pos) => {
        console.log('gameState', state);
        checkMoving(selected, pos);
      });

    	function func(row, column) {
    		const $$result = pick = `${row}_${column}`;
    		$$invalidate('pick', pick);
    		return $$result;
    	}

    	return { jewels, pick, bgColors, func };
    }

    class GameField extends SvelteComponentDev {
    	constructor(options) {
    		super(options);
    		init(this, options, instance$3, create_fragment$3, safe_not_equal, []);
    	}
    }

    /* src/App.svelte generated by Svelte v3.4.4 */

    const file$3 = "src/App.svelte";

    function create_fragment$4(ctx) {
    	var t0, h1, t1, t2, t3, t4, t5, current;

    	var animate = new Animate({ $$inline: true });

    	var counter = new Counter({ $$inline: true });

    	var gamefield = new GameField({ $$inline: true });

    	return {
    		c: function create() {
    			animate.$$.fragment.c();
    			t0 = space();
    			h1 = element("h1");
    			t1 = text("Hello ");
    			t2 = text(ctx.name);
    			t3 = text("!");
    			t4 = space();
    			counter.$$.fragment.c();
    			t5 = space();
    			gamefield.$$.fragment.c();
    			h1.className = "svelte-i7qo5m";
    			add_location(h1, file$3, 1, 0, 12);
    		},

    		l: function claim(nodes) {
    			throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
    		},

    		m: function mount(target, anchor) {
    			mount_component(animate, target, anchor);
    			insert(target, t0, anchor);
    			insert(target, h1, anchor);
    			append(h1, t1);
    			append(h1, t2);
    			append(h1, t3);
    			insert(target, t4, anchor);
    			mount_component(counter, target, anchor);
    			insert(target, t5, anchor);
    			mount_component(gamefield, target, anchor);
    			current = true;
    		},

    		p: function update(changed, ctx) {
    			if (!current || changed.name) {
    				set_data(t2, ctx.name);
    			}
    		},

    		i: function intro(local) {
    			if (current) return;
    			animate.$$.fragment.i(local);

    			counter.$$.fragment.i(local);

    			gamefield.$$.fragment.i(local);

    			current = true;
    		},

    		o: function outro(local) {
    			animate.$$.fragment.o(local);
    			counter.$$.fragment.o(local);
    			gamefield.$$.fragment.o(local);
    			current = false;
    		},

    		d: function destroy(detaching) {
    			animate.$destroy(detaching);

    			if (detaching) {
    				detach(t0);
    				detach(h1);
    				detach(t4);
    			}

    			counter.$destroy(detaching);

    			if (detaching) {
    				detach(t5);
    			}

    			gamefield.$destroy(detaching);
    		}
    	};
    }

    function instance$4($$self, $$props, $$invalidate) {
    	

    	let { name } = $$props;

    	const writable_props = ['name'];
    	Object.keys($$props).forEach(key => {
    		if (!writable_props.includes(key) && !key.startsWith('$$')) console.warn(`<App> was created with unknown prop '${key}'`);
    	});

    	$$self.$set = $$props => {
    		if ('name' in $$props) $$invalidate('name', name = $$props.name);
    	};

    	return { name };
    }

    class App extends SvelteComponentDev {
    	constructor(options) {
    		super(options);
    		init(this, options, instance$4, create_fragment$4, safe_not_equal, ["name"]);

    		const { ctx } = this.$$;
    		const props = options.props || {};
    		if (ctx.name === undefined && !('name' in props)) {
    			console.warn("<App> was created without expected prop 'name'");
    		}
    	}

    	get name() {
    		throw new Error("<App>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	set name(value) {
    		throw new Error("<App>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}
    }

    const app = new App({
    	target: document.body,
    	props: {
    		name: 'world'
    	}
    });

    return app;

}());
//# sourceMappingURL=bundle.js.map
